﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LaunchMenu : MonoBehaviour
{
    public bool serverMode = true;
    public bool skipMenu = false;
    //public static IPAddress ip = IPAddress.Loopback; //127.0.0.1
    //private static IPAddress ipDef = IPAddress.Loopback;
    public static string ip = "127.0.0.1";
    private static string ipDef = "127.0.0.1";
    public static ushort port = 9000;
    private static ushort portDef = 9000;
    public static string serverSceneName = "ServerLevel";
    public static string clientSceneName = "Level01ECS";
    void Start(){
        DontDestroyOnLoad(this);
        ip = ipDef;
        port = portDef;
        //handle launch line arguments
        string[] args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++){
            //skipMenu to avoid having both client and server
            if(!skipMenu && args[i] == "-server" || args[i] == "-batchmode"){ //batchmode defaults to server launch
                serverMode = true;
                skipMenu = true;
                Debug.Log("LaunchMenu : launching in server mode");
            }if(!skipMenu && args[i] == "-client"){
                serverMode = false;
                skipMenu = true;
                Debug.Log("LaunchMenu : launching in client mode");
            }if (args [i] == "-port") {
                port = ParsePort(args[i + 1], portDef);
                
            }if (args [i] == "-ip") {
                ip = ParseIP(args[i+1],ipDef);
            }
        }
        if(!skipMenu){
            //menu doesnt work in batchmode so batchmode always skips (to server if unspecified)
            Debug.Log("LaunchMenu : not skipping menu this time !");
        }else{
            if(serverMode){
                Debug.Log("LaunchMenu : skipping to server scene");
                SceneManager.LoadScene(serverSceneName);
            }else{
                Debug.Log("LaunchMenu : skipping to client scene");
                SceneManager.LoadScene(clientSceneName);
            }
        }

    }

    ushort ParsePort(string portStr, ushort def){
        if(ushort.TryParse(portStr, out ushort j) && j>0 && j<65535){
            Debug.Log("LaunchMenu : port passed as arg is : "+portStr);
            return j;
        }else{
            Debug.Log("LaunchMenu : no port passed or invalid, using default value "+def);
            return def;
        }
    }

    string ParseIP(string ipStr, string def){
        if(IPAddress.TryParse(ipStr, out IPAddress j)){ //try parsing ipaddress format but return a string if possible
            Debug.Log("LaunchMenu : ip passed as arg is : "+ipStr);
            return ipStr;
        }else{
            Debug.Log("LaunchMenu : no ip passed or invalid, using default value "+def.ToString());
            return def;
         }
    }

    void ParseTextFields(){
        ip = ParseIP(GameObject.Find("IPText").GetComponent<Text>().text, ipDef);
        port = ParsePort(GameObject.Find("PortText").GetComponent<Text>().text, portDef);
    }

    public void LaunchScene(string sceneName){
        if(sceneName == "server"){
            Debug.Log("LaunchMenu : launching server scene");
            ParseTextFields();
            SceneManager.LoadScene(serverSceneName);
        }else if(sceneName == "client"){
            Debug.Log("LaunchMenu : launching  client scene");
            ParseTextFields();
            SceneManager.LoadScene(clientSceneName);
        }
    }
    public void Quit(){
        Application.Quit();
    }
}
