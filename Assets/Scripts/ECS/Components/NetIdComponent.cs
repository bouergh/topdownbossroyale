using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using System;

public struct NetIdComponent : IComponentData
{
    public uint id;
}