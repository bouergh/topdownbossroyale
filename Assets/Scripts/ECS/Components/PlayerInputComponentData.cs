using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using System;

// public struct PlayerInputComponentData : IComponentData
// {
//     public float2 MoveDirection;
//     public float2 AimPosition;
//     public bool Shoot;
// }

//version from multiplayer asteroid example
public unsafe struct PlayerInputComponentData : IComponentData
{
    public int mostRecentPos;
    public fixed uint tick[32];
    public fixed byte left[32];
    public fixed byte right[32];
    public fixed byte thrust[32];
    public fixed byte shoot[32];




    public float2 MoveDirection;
    public float2 AimPosition;
    public bool Shoot;
}