﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

// Serializable attribute is for editor support.
[Serializable]
public struct TranslationSpeed : IComponentData
{
    public float MetersPerSecond;
}
