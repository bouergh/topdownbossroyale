using Unity.Entities;

public struct PlayerSpawner : IComponentData
{
    public int CountX;
    public int CountY;
    public Entity Prefab;
}    