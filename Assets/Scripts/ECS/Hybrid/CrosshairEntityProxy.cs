﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[RequiresEntityConversion]
public class CrosshairEntityProxy : MonoBehaviour, IConvertGameObjectToEntity
{
    
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponent(entity, typeof(PlayerInputComponentData));
        dstManager.AddComponent(entity, typeof(CrosshairData));
        //dstManager.SetName(entity, gameObject.name);
    }
}
