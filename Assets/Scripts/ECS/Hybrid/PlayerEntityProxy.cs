﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[RequiresEntityConversion]
// [UpdateInGroup(typeof(ClientSimulationSystemGroup))]
public class PlayerEntityProxy : MonoBehaviour, IConvertGameObjectToEntity
{
    public float UnitPerSecond = 1;
    public uint netId;
    
    
    
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        // conversionSystem.DstEntityManager = ClientServerBootstrap.clientWorld[0].EntityManager;
        // conversionSystem.DstWorld = ClientServerBootstrap.clientWorld[0];
        // conversionSystem.CreateAdditionalEntity(this);

        var data = new TranslationSpeed { MetersPerSecond = UnitPerSecond };
        dstManager.AddComponentData(entity, data);
        var dataId = new NetIdComponent{id = netId};
        dstManager.AddComponentData(entity, dataId);
        dstManager.AddComponent(entity, typeof(PlayerInputComponentData));
        // to test asteroids input
        // dstManager.AddComponent(entity, typeof(PlayerStateComponentData));

        
        //ClientServerBootstrap.clientWorld[0].EntityManager.MoveEntitiesFrom(dstManager);
        // EntityManager mg = ClientServerBootstrap.clientWorld[0].EntityManager;
        // var ent = GameObjectConversionUtility.ConvertGameObjectHierarchy(gameObject,mg.World);
        // mg.AddComponent(ent,typeof(Rigidbody2D));
    }
}