﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


//[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
public class TranslationSpeedSystem : JobComponentSystem
{
    // Use the [BurstCompile] attribute to compile a job with Burst. You may see significant speed ups, so try it!
    //[BurstCompile]
    struct TranslationSpeedJob : IJobForEach<Translation, TranslationSpeed, PlayerInputComponentData>
    {
        public float DeltaTime;

        public void Execute(ref Translation translation, ref TranslationSpeed trnSpeed, ref PlayerInputComponentData input)
        {
            var move = math.normalizesafe(new float3(input.MoveDirection.x,input.MoveDirection.y, 0));
            translation.Value = translation.Value + move*trnSpeed.MetersPerSecond * DeltaTime;
        }
    }

    // OnUpdate runs on the main thread.
    protected override JobHandle OnUpdate(JobHandle inputDependencies)
    {
        var job = new TranslationSpeedJob()
        {
            DeltaTime = Time.deltaTime
        };
        return job.Schedule(this, inputDependencies);
    }
}