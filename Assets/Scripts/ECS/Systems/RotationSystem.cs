using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
public class RotationSystem : ComponentSystem {
    protected override void OnUpdate() {
        Entities.ForEach((Transform transform, ref Rotation rotation) => {
            transform.rotation = rotation.Value;
        });
    }
}