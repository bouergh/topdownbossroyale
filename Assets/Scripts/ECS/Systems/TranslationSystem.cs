using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

//[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
public class TranslationSystem : ComponentSystem {
    protected override void OnUpdate() {
        Entities.ForEach((Transform transform, ref Translation translation) => {
            transform.position = new Vector3(translation.Value.x, translation.Value.y, translation.Value.z);
        });
    }
}