using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine.Experimental.Input;
using Unity.Mathematics;


//[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
public class InputSystem : ComponentSystem //looks like its better to do inputcollection on main thread only
{
    InputMaster im;
    static float2 move = float2.zero;
    static float2 aim = float2.zero;
    static bool fire = false;

    protected override void OnCreate(){
        base.OnCreate();
        im = new InputMaster();
        im.Player.Enable();
        im.Player.Move.started += ctx => OnMove(ctx);
        im.Player.Move.performed += ctx => OnMove(ctx);
        im.Player.Move.cancelled += ctx => OnMove(ctx);
        im.Player.Aim.started += ctx => OnAim(ctx);
        im.Player.Aim.performed += ctx => OnAim(ctx);
        im.Player.Aim.cancelled += ctx => OnAim(ctx);
        im.Player.Fire.started += ctx => OnFire(ctx);
        im.Player.Fire.performed += ctx => OnFire(ctx);
        im.Player.Fire.cancelled += ctx => OnFire(ctx);
    }

    protected override void OnUpdate() {
        Entities.ForEach((ref PlayerInputComponentData input) => {
            input.MoveDirection = move;
            input.Shoot = fire;
            input.AimPosition = aim;
        });
    }
    private static void OnMove(InputAction.CallbackContext ctx){
        move = ctx.ReadValue<Vector2>();
    }
    private static void OnAim(InputAction.CallbackContext ctx){
        aim = ctx.ReadValue<Vector2>();
        aim = (float2)(Vector2)Camera.main.ScreenToWorldPoint(new Vector2(aim.x, aim.y));
        //Debug.Log("aim is "+aim); //a l'air correct mais rien de visible ingame pour l'instant
    }
    private static void OnFire(InputAction.CallbackContext ctx){
        fire = (ctx.ReadValue<float>() == 1);
        //Debug.Log("fire is "+fire);
    }

    //old inputsystem version when cannot instantiate InputMaster
    // protected override void OnUpdate() {
    //     Entities.ForEach((ref PlayerInputComponentData input) => {
    //         input.MoveDirection = WasdInput();
    //         input.Shoot = ClickInput();
    //         input.AimPosition = MousePosition();
    //     });
    // }

    
    // private static float2 WasdInput(){
    //     float w = Keyboard.current.wKey.isPressed ? 1f : 0f;
    //     float a = Keyboard.current.aKey.isPressed ? -1f : 0f;
    //     float s = Keyboard.current.sKey.isPressed ? -1f : 0f;
    //     float d = Keyboard.current.dKey.isPressed ? 1f : 0f;
    //     return new float2(d+a,w+s);
    // }

    // private static bool ClickInput(){
    //     return Mouse.current.leftButton.isPressed;
    // }

    // private static float2 MousePosition(){
    //     Vector3 v3pos = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
    //     return new float2(v3pos.x, v3pos.y);
    // }
}