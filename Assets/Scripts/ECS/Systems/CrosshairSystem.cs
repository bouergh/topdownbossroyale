﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class CrosshairSystem : JobComponentSystem
{
    // Use the [BurstCompile] attribute to compile a job with Burst. You may see significant speed ups, so try it!
    //[BurstCompile]
    
    struct CrosshairJob : IJobForEach<Translation, CrosshairData, PlayerInputComponentData>
    {
        
        public void Execute(ref Translation translation, [ReadOnly] ref CrosshairData crdt, ref PlayerInputComponentData input)
        {
            translation.Value = new float3(input.AimPosition.x,input.AimPosition.y, 0);
        }
    }

    // OnUpdate runs on the main thread.
    protected override JobHandle OnUpdate(JobHandle inputDependencies)
    {
        var job = new CrosshairJob(){};
        return job.Schedule(this, inputDependencies);
    }
}