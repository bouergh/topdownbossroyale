﻿using System.Net;
using UnityEngine;

using Unity.Networking.Transport;
using Unity.Collections;
using NetworkConnection = Unity.Networking.Transport.NetworkConnection; 
using Unity.Jobs;
using UnityEngine.Assertions;
using Unity.Entities;

struct ServerUpdateConnectionsJob : IJob
{
    public UdpNetworkDriver driver;
    public NativeList<NetworkConnection> connections;
    
    public void Execute()
    {
        // Clean up connections
        for (int i = 0; i < connections.Length; i++)
        {
            if (!connections[i].IsCreated)
            {
                connections.RemoveAtSwapBack(i);
                --i;
            }
        }
        // Accept new connections
        NetworkConnection c;
        while ((c = driver.Accept()) != default(NetworkConnection))
        {
            connections.Add(c);
            Debug.Log("Accepted a connection");
        }
    }
}

struct ServerUpdateJob : IJobParallelFor
{
    public UdpNetworkDriver.Concurrent driver;
    public NativeArray<NetworkConnection> connections;
    
    public NativeArray<int> playerSpawned;
    

    public void Execute(int index)
    {
        DataStreamReader stream;
        if (!connections[index].IsCreated)
            Assert.IsTrue(true);

        NetworkEvent.Type cmd;
        while ((cmd = driver.PopEventForConnection(connections[index], out stream)) !=
        NetworkEvent.Type.Empty)
        {
            if (cmd == NetworkEvent.Type.Data)
            {
                var readerCtx = default(DataStreamReader.Context);
                uint number = stream.ReadUInt(ref readerCtx);
                
                // Debug.Log("Got " + number + " from the Client adding + 2 to it.");
                // number +=2;

                if(number == 4){
                    Debug.Log("Got code 4 (connection OK) from the Client. Try instantiate player");
                    int i = 0;
                    bool found = false;
                    while(!found && i<playerSpawned.Length){
                        if(playerSpawned[i]==0){
                            playerSpawned[i]=-1; //choose a netid for new player !
                            found = true;
                        }
                        i++;
                    }
                    if(found){
                        using (var writer = new DataStreamWriter(4, Allocator.Temp))
                        {
                            Debug.Log("Slot "+i+" available, sending RPC");
                            writer.Write(5); //send same netid for player to have on the client
                            driver.Send(NetworkPipeline.Null, connections[index], writer);
                        }
                    }else{
                        Debug.Log("No slot left for player to spawn !");
                    } 

                    
                }

                
            }
            else if (cmd == NetworkEvent.Type.Disconnect)
            {
                Debug.Log("Client disconnected from server");
                connections[index] = default(NetworkConnection);
            }
        }
    }   
}

public class JobifiedServerBehaviour : MonoBehaviour
{

    public UdpNetworkDriver m_Driver;
    public NativeList<NetworkConnection> m_Connections;
    private JobHandle ServerJobHandle;
	public GameObject playerPrefab;
    public EntityCommandBuffer lol;
    
	public NativeArray<int> m_PlayerSpawned; //0 for unspawned, -1 for waiting to spawn, 1 for already spawned
    void Start()
    {
        m_PlayerSpawned = new NativeArray<int>(4, Allocator.Persistent);
        Debug.Log("Server scene launched, using ip "+LaunchMenu.ip+" and port "+LaunchMenu.port);       
        var addr = NetworkEndPoint.Parse(LaunchMenu.ip,LaunchMenu.port);

        m_Driver = new UdpNetworkDriver(new INetworkParameter[0]);
        if(m_Driver.Bind(addr) != 0) 
            Debug.Log("Failed to bind to port 9000");
        else
            m_Driver.Listen();
        
        m_Connections = new NativeList<NetworkConnection>(16, Allocator.Persistent);
        
    }

    void OnDestroy()
    {
        ServerJobHandle.Complete();
        m_Driver.Dispose();
        m_Connections.Dispose();
        m_PlayerSpawned.Dispose();
    }

    // Update is called once per frame
    void Update()
    {
        ServerJobHandle.Complete();
        for(int i = 0; i< m_PlayerSpawned.Length; i++)
        {
            if(m_PlayerSpawned[i]<0){
                Spawn(i);
                m_PlayerSpawned[i]=1;
            }
        }

        var connectionJob = new ServerUpdateConnectionsJob
        {
            driver = m_Driver, 
            connections = m_Connections,
            
        };

        var serverUpdateJob = new ServerUpdateJob
        {
            driver = m_Driver.ToConcurrent(),
            connections = m_Connections.AsDeferredJobArray(),
            playerSpawned = m_PlayerSpawned
        };
        
        ServerJobHandle = m_Driver.ScheduleUpdate();
        ServerJobHandle = connectionJob.Schedule(ServerJobHandle);
        //Schedule(m_Connections, 1, ServerJobHandle);  not working, wants (idky) a NativeHashMap instead of NativeList<NetworkConnection>, but it makes sense to pass the Length instead ?
        ServerJobHandle = serverUpdateJob.Schedule(m_Connections.Length, 1, ServerJobHandle); 
        
    }

    void Spawn(int i){
		var go = GameObject.Instantiate(playerPrefab);
        go.GetComponent<PlayerEntityProxy>().netId = (uint)1111;
        Debug.Log("Player "+i+" spawned on server");
	}
}
