﻿using System.Net;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using Unity.Networking.Transport;



struct ClientUpdateJob : IJob
{
	public UdpNetworkDriver driver;
	public NativeArray<NetworkConnection> connection;
	public NativeArray<byte> done;
	public NativeArray<int> playerSpawned;

	public void Execute()
	{
		if (!connection[0].IsCreated)
		{
			if (done[0] != 1)
				Debug.Log("Something went wrong during connect");
			return;
		}
		
		DataStreamReader stream;
		NetworkEvent.Type cmd;
		
		while ((cmd = connection[0].PopEvent(driver, out stream)) != 
			   NetworkEvent.Type.Empty)
		{
			if (cmd == NetworkEvent.Type.Connect)
			{
				Debug.Log("We are now connected to the server");

				var value = 4;
				using (var writer = new DataStreamWriter(4, Allocator.Temp))
				{
					writer.Write(value);
					connection[0].Send(driver, writer);
				}
			}
			else if (cmd == NetworkEvent.Type.Data)
			{
				var readerCtx = default(DataStreamReader.Context);
				uint value = stream.ReadUInt(ref readerCtx);
				Debug.Log("Got code 5 (connection OK) from the Server. Instantiating player.");
				playerSpawned[0] = (int)value;
				done[0] = 1;
				//Debug.Log("Got the value = " + value + " back from the server");
				// And finally change the `done[0]` to `1`
				// done[0] = 1;
				// connection[0].Disconnect(driver);
				// connection[0] = default(NetworkConnection);
			}
			else if (cmd == NetworkEvent.Type.Disconnect)
			{
				Debug.Log("Client got disconnected from server");
				connection[0] = default(NetworkConnection);
			}
		}
	}
}


public class JobifiedClientBehaviour : MonoBehaviour
{
    public UdpNetworkDriver m_Driver;
    public NativeArray<NetworkConnection> m_Connection;
    public NativeArray<byte> m_Done;
    
    public JobHandle ClientJobHandle;
	public GameObject player, crosshair;
	public NativeArray<int> m_PlayerSpawned;
	private bool alreadySpawned = false;
    
    void Start ()
	{
		m_PlayerSpawned = new NativeArray<int>(1, Allocator.Persistent);
		m_PlayerSpawned[0] = -1; //will be replaced by uint id
		Debug.Log("Client scene launched, using ip "+LaunchMenu.ip+" and port "+LaunchMenu.port);
        m_Driver = new UdpNetworkDriver(new INetworkParameter[0]);
        
        m_Connection = new NativeArray<NetworkConnection>(1, Allocator.Persistent);
        m_Done = new NativeArray<byte>(1, Allocator.Persistent);
		
		var addr = NetworkEndPoint.Parse(LaunchMenu.ip,LaunchMenu.port);
		//var addr = NetworkEndPoint.LoopbackIpv4;
        m_Connection[0] = m_Driver.Connect(addr);

		
    }
    
    public void OnDestroy()
    {
        ClientJobHandle.Complete();
        m_Connection.Dispose();
        m_Driver.Dispose();
        m_Done.Dispose();
    }
	
    void Update()
    {
        ClientJobHandle.Complete();
		if(!alreadySpawned && m_PlayerSpawned[0] != -1){
			Spawn((uint)m_PlayerSpawned[0]);
			alreadySpawned = true;
		}
        var job = new ClientUpdateJob
        {
            driver = m_Driver,
            connection = m_Connection,
            done = m_Done,
			playerSpawned = m_PlayerSpawned
        };
        ClientJobHandle = m_Driver.ScheduleUpdate();
        ClientJobHandle = job.Schedule(ClientJobHandle);
    }

	void Spawn(uint netId){
		var go = GameObject.Instantiate(player);
		go.GetComponent<PlayerEntityProxy>().netId = netId;
		GameObject.Instantiate(crosshair);

	}
}
