using UnityEditor;
using UnityEngine;
using System;
using Windows;
public class ConsoleSystem : MonoBehaviour
{
//#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
//bugs in Editor + not useful in it right now. Errors with redraw, not showing anything, and Editor crashed when closing the console
#if UNITY_STANDALONE_WIN

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        
    }

    public static void Run(string obj, bool fromServerConsole){
        if(fromServerConsole){
            Debug.Log("Following command has been run through console : "+obj);
            if(obj == "ip"){
                Debug.Log("ip : "+LaunchMenu.ip+"\nport : "+LaunchMenu.port);
            }else{
		        Debug.Log("YEET ! Not a valid console command !");  
            }
            
        }
    }

#endif
}