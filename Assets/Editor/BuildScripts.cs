﻿using UnityEditor;
using UnityEngine;
using UnityEditor.Build.Reporting;
using System.Diagnostics;

// Output the build size or a failure depending on BuildPlayer.

public class BuildScripts : MonoBehaviour
{

    public static bool Build(string path)
    {
        //if(path=="") path = basePath+"TDBR.exe";
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] {"Assets/Scene/StartScreen.unity", "Assets/Scene/Level01ECS.unity", "Assets/Scene/ServerLevel.unity"};
        buildPlayerOptions.locationPathName = path;
        buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
        buildPlayerOptions.options = BuildOptions.None;

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            UnityEngine.Debug.Log("Build succeeded: " + summary.totalSize + " bytes in "+path);
            return true;
        }

        if (summary.result == BuildResult.Failed)
        {
            UnityEngine.Debug.Log("Build failed");
            return false;
        }
        return false;

    }

    
    public static void LaunchBuild(string path, string args){
        Process build = new Process();
        build.StartInfo.FileName = path;
        build.StartInfo.Arguments = args;
        build.Start();
    }


    //following is deprecated ? maybe not, see later
    //[MenuItem("Build/Build&Run Windows Server")]
    /*
    public static void ServerBuild(string path)
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scene/ServerLevel.unity" };
        buildPlayerOptions.locationPathName = path;
        buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
        buildPlayerOptions.options = BuildOptions.EnableHeadlessMode;

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            UnityEngine.Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
        }

        if (summary.result == BuildResult.Failed)
        {
            UnityEngine.Debug.Log("Build failed");
        }
    }

    //[MenuItem("Build/Build&Run Windows Client")]
    public static void ClientBuild(string path)
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scene/Level01.unity" };
        buildPlayerOptions.locationPathName = path;
        buildPlayerOptions.target = BuildTarget.StandaloneWindows64;

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            UnityEngine.Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
        }

        if (summary.result == BuildResult.Failed)
        {
            UnityEngine.Debug.Log("Build failed");
        }
    }
     */


    /* 
    //[MenuItem("Build/Build&Run Linux Server")]
    public static void ServerLinuxBuild()
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scene/ServerLevel.unity" };
        buildPlayerOptions.locationPathName = "C:/Gitlab-Runner/Builds/Linux_Server/TDBR_Server";
        buildPlayerOptions.target = BuildTarget.StandaloneLinux64;
        buildPlayerOptions.options = BuildOptions.EnableHeadlessMode;

        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            UnityEngine.Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
            Process.Start(buildPlayerOptions.locationPathName);
        }

        if (summary.result == BuildResult.Failed)
        {
            UnityEngine.Debug.Log("Build failed");
        }
    }
    */
}