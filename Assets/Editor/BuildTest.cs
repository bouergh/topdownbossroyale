using UnityEditor;
using UnityEngine;
using System.Net;

public class BuildTest : EditorWindow
{
    public string buildPath = "S:/Bouergh/Documents/Unity/TDBR_Builds/";
    string fileName = "TDBR.exe";
    int clientNumber = 2;
    int portNumber = 9000;
    string ipString = "localhost";
    bool runServer = true;
    
    // Add menu item named "Builds and Test" to the Window menu
    [MenuItem("Window/Builds and Test")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(BuildTest));
    }

    bool Build(){
        return BuildScripts.Build(buildPath+'/'+fileName);
    }
    void Run(){
        if(runServer)
            BuildScripts.LaunchBuild(buildPath+'/'+fileName,"-server -ip "+ipString+" -port "+portNumber);
        for(int i = 0; i<clientNumber; i++){
            BuildScripts.LaunchBuild(buildPath+'/'+fileName,"-client -ip "+ipString+" -port "+portNumber);
        }
    }
    
    void OnGUI()
    {

        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("Build Folder")){
            buildPath = EditorUtility.OpenFolderPanel("Select Build Folder", "", "");
        }
        buildPath = EditorGUILayout.TextField ("", buildPath);
        EditorGUILayout.EndHorizontal();


        EditorGUILayout.BeginHorizontal();
        clientNumber = EditorGUILayout.IntField("Number of client to run", clientNumber);
        runServer = EditorGUILayout.Toggle("Run a server ?",runServer);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        ipString = EditorGUILayout.TextField("IP and Port", ipString);
        portNumber = EditorGUILayout.IntField("", portNumber);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("Build")){
            Build();
        }
        if(GUILayout.Button("Build and Run")){
            if(Build()){
                Run();
            }
        }
        if(GUILayout.Button("Run")){
           Run();
        }
        EditorGUILayout.EndHorizontal();

    }
}